<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Like extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $twit;
    public function __construct($twit)
    {
        $this->twit=$twit;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.like');
    }
}
