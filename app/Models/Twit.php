<?php

namespace App\Models;

use App\Models\Trait\HasLike;
use App\Models\Trait\Likeable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Twit extends Model
{
    use HasFactory,Likeable;

    protected $table = 'twits';
    protected $guarded = false;
    use softDeletes;

    public $timestamps = true;

    public function author(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function hasUserLike($user_id)
    {
        $x = Like::where('user_id', '=', $user_id)->where('twit_id', '=', $this->id)->get();
        if ($x) {
            foreach ($x as $value) {
                if ($value->user_id === $user_id) {
                    return true;
                }
            }
        }
        return false;
    }

    public function like()
    {
        $x = Like::where('user_id', '=', Auth::id())->where('twit_id', '=', $this->id)->count();
        if ($x != 0) {
            $x = Like::where('user_id', '=', Auth::id())->where('twit_id', '=', $this->id)->get();
            foreach ($x as $value) {
                return 1;
            }
        }
        return 0;
    }
}
