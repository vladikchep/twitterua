<?php

namespace App\Models\Trait;

use App\Models\User;

trait Followable
{
     /**
     * Follow the given user.
     *
     * @param User $user
     * @return mixed
     */
    public function follow(User $user)
    {
        if (! $this->isFollowing($user) && $this->id != $user->id)
        {
             $this->followingOn()->attach($user);
        }
        return null;
    }

    /**
     * Unfollow the given user.
     *
     * @param User $user
     * @return mixed
     */
    public function unFollow(User $user)
    {
        return $this->followingOn()->detach($user);
    }

    /**
     * Get all the users that this user is following.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function followingOn()
    {
        return $this->belongsToMany(User::class, 'followers', 'follower_id', 'user_id')->withTimestamps();
    }

    /**
     * Get all the users that are following this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function followers()
    {
        return $this->belongsToMany(User::class, 'followers', 'user_id', 'follower_id')->withTimestamps();
    }

    /**
     * Check if a given user is following this user.
     *
     * @param User $user
     * @return bool
     */
    public function isFollowing(User $user)
    {
        return !! $this->followingOn()->where('user_id', $user->id)->count();
    }

    /**
     * Check if a given user is being followed by this user.
     *
     * @param User $user
     * @return bool
     */
    public function isFollowedBy(User $user)
    {
        return !! $this->followers()->where('follower_id', $user->id)->count();
    }
}
