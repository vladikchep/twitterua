<?php

namespace App\Models\Trait;

use App\Models\Twit;

trait HasLike
{
    /**
     * Like the given twit.
     *
     * @param Twit $twit
     * @return mixed
     */
    public function like(Twit $twit)
    {
        if (! $this->hasLiked($twit))
        {
             $this->likes()->attach($twit);
        }
        return null;
    }

    /**
     * Unlike the given twit.
     *
     * @param Twit $twit
     * @return mixed
     */
    public function unlike(Twit $twit)
    {
        return $this->likes()->detach($twit);
    }

    /**
     * Get the twits liked by the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function likes()
    {
        return $this->belongsToMany(Twit::class, 'likes', 'user_id', 'twit_id')->withTimestamps();
    }

    /**
     * Check if the user has liked the given twit.
     *
     * @param Twit $twit
     * @return bool
     */
    public function hasLiked(Twit $twit)
    {
        return !! $this->likes()->where('twit_id', $twit->id)->count();
    }
}
