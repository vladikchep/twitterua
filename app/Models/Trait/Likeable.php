<?php

namespace App\Models\Trait;

use App\Models\Twit;
use App\Models\User;

trait Likeable
{
     /**
     * Get the users that liked the twit.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function liked()
    {
        return $this->belongsToMany(User::class, 'likes', 'twit_id', 'user_id')->withTimestamps();
    }

    /**
     * Check if the twit is liked by the given user.
     *
     * @param User $user
     * @return bool
     */
    public function isLikedBy(User $user)
    {
        return !! $this->liked()->where('user_id', $user->id)->count();
    }

}
