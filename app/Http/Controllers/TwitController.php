<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditTwitRequest;
use App\Http\Requests\NewTwitRequest;
use App\Models\Twit;
use App\Services\twitService;


class TwitController extends Controller
{
    public function __construct(twitService $twitService)
    {
        $this->twitService=$twitService;
    }
    public function index(){
        $twits=$this->twitService->getAll();
        return view('twit.index',)->with('twits',$twits);
    }
    public function create(){
        return view('twit.create');
    }
    public function store(NewTwitRequest $request,Twit $twit){
        $this->twitService->store($request);
        return redirect()->route('twit.store');
    }
    public function edit(EditTwitRequest $request,Twit $twit){

        return view('twit.edit')->with('twit',$twit);
    }
    public function update(EditTwitRequest $request,Twit $twit){
        $this->twitService->update($request,$twit);
        return redirect()->route('twit.index');
    }
    public function destroy(Twit $twit){
        $twit->delete();
        return redirect()->route('twit.index');
    }
}
