<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class FollowerController extends Controller
{
    public function follow(Request $request){
        $user=User::find($request->user_id);
        $authenticatedUser = auth()->user();
        if($request->followed==0){
            $follow=$authenticatedUser->follow($user);
        } else {
            $follow=$authenticatedUser->unFollow($user);

        }
        return response()->json(['data' => $follow]);
    }
}
