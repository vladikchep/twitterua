<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Providers\RouteServiceProvider;
use App\Services\userService;


class RegisterController extends Controller
{


    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(userService $userService)
    {
        $this->middleware('guest');
        $this->userService=$userService;
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  RegisterRequest $request
     * @return \App\Models\User
     */
    protected function create(RegisterRequest $request)
    {
        return $this->userService->store($request);
    }
}
