<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use App\Services\userService;


class UserController extends Controller
{
    public function __construct(userService $userService)
    {
        $this->userService=$userService;
    }

    public function show(User $user)
    {
        return view('user.index', compact('user'));
    }

    public function edit(User $user, UserUpdateRequest $request)
    {
        return view('user.edit', compact('user'));
    }

    public function update(User $user, UserUpdateRequest $request)
    {
        $this->userService->update($request);
        return redirect()->route('user.index', $user->id);
    }

}


