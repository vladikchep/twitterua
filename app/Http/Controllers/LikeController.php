<?php

namespace App\Http\Controllers;

use App\Models\Twit;
use App\Services\likeService;
use Illuminate\Http\Request;

class LikeController extends Controller
{
    public function __construct(likeService $likeService)
    {
        $this->likeService = $likeService;
    }

    public function like(Request $request)
    {
        $user = auth()->user();
        $twit = Twit::find($request->twit_id);
        if ((int)$request->action == 0) {
            $like = $user->like($twit);
        } else {
            $like = $user->unlike($twit);
        }
        return response()->json(['data' => $like]);
    }
}
