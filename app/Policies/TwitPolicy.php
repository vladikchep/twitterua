<?php

namespace App\Policies;

use App\Models\Twit;
use App\Models\User;
use Illuminate\Auth\Access\Response;
use Illuminate\Auth\Access\HandlesAuthorization;

class TwitPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function view(User $user, Twit $twits)
    {
            if (!$twits->only_for_followers&&$twits->user_id!=$user->id) {
                return $user->isFollowing(User::find($twits->user_id)) ? Response::allow() : Response::deny("no");
            } else {
                return Response::allow();
            }
    }
    /**
     * Determine whether the user can update the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\User $model
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Twit $twit)
    {
        return $user->id === $twit->author->id ? Response::allow() : Response::deny("no");
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\User $model
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Twit $twit)
    {
        return $user->id === $twit->author->id ? Response::allow() : Response::deny("no");
    }

}
