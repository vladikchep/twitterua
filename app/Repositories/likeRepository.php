<?php

namespace App\Repositories;

use App\Models\Like;
use Illuminate\Support\Facades\Auth;

class likeRepository
{
    public function all()
    {
        return Like::all();
    }

    public function create($data)
    {
        return Like::firstOrCreate(
            ['twit_id' => (int)$data, 'user_id' => Auth::id()]);
    }

    public function delete($data)
    {
        return Like::where('user_id', '=', Auth::id())->where('twit_id', '=', (int)$data)->delete();
    }

}
