<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class userRepository
{
    public function create($data)
    {
        return User::create($data);
    }

    public function update($data)
    {
        return Auth::user()->update($data);
    }

}
