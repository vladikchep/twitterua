<?php

namespace App\Repositories;

use App\Models\Twit;
use Illuminate\Support\Facades\Auth;

class twitRepository
{
    public function all()
    {
        return Twit::with('author')->get();
    }

    public function create($data)
    {
        $data['user_id'] = Auth::id();
        return Twit::create($data);
    }
    public function update($data,$twit)
    {
        return $twit->update($data);
    }
}
