<?php

namespace App\Services;

use App\Repositories\twitRepository;

class twitService
{
    public function __construct()
    {
        $this->twitRepository = new twitRepository();
    }

    public function getAll()
    {
        return $this->twitRepository->all();
    }

    public function store($data)
    {
        $data = $data->validated();
        return $this->twitRepository->create($data);
    }
    public function update($data,$twit)
    {
        $data = $data->validated();
        return $this->twitRepository->update($data,$twit);
    }

}
