<?php

namespace App\Services;

use App\Repositories\likeRepository;


class likeService
{
    public function __construct()
    {
        $this->likeRepository = new likeRepository();
    }

    public function getAll()
    {
        return $this->likeRepository->all();
    }

    public function store($data)
    {
        return $this->likeRepository->create($data);
    }
    public function delete($data)
    {
        return $this->likeRepository->delete($data);
    }


}
