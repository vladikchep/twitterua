<?php

namespace App\Services;

use App\Repositories\userRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class userService
{
    public function __construct()
    {
        $this->userRepository = new userRepository();
    }


    public function store($request)
    {
        $val = $request->validated();
        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');;
            $name = md5(Carbon::now() . '_' . $avatar->getClientOriginalName()) . '.' . $avatar->getClientOriginalExtension();
            $filePath = Storage::disk('public')->putFileAs('/images/avatars', $avatar, $name);
            $thumbnail_name = 'thumb_' . $name;
            Image::make($avatar)->fit(100, 100)->save(storage_path('app/public/images/avatars/' . $thumbnail_name));
            $avatar_arr = [
                'avatar_path' => $filePath,
                'avatar_url' => url('/storage/' . $filePath),
                'avatar_url_thumb' => url('/storage/images/avatars/' . $thumbnail_name),
            ];
            $val = array_merge($val, $avatar_arr);
        }
        $val['password'] = Hash::make($val['password']);
        return $this->userRepository->create($val);
    }

    public function update($request)
    {
        $val = $request->validated();
        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $name = md5(Carbon::now() . '_' . $avatar->getClientOriginalName()) . '.' . $avatar->getClientOriginalExtension();
            $filePath = Storage::disk('public')->putFileAs('/images/avatars', $avatar, $name);
            $thumbnail_name = 'thumb_' . $name;
            Image::make($avatar)->fit(100, 100)->save(storage_path('app/public/images/avatars/' . $thumbnail_name));
            $avatar_arr = [
                'avatar_path' => $filePath,
                'avatar_url' => url('/storage/' . $filePath),
                'avatar_url_thumb' => url('/storage/images/avatars/' . $thumbnail_name),
            ];
            $val = array_merge($val, $avatar_arr);
        }
        return $this->userRepository->update($val);
    }

}
