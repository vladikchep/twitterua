<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('followers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->index('user_id', 'user_follow_idx');
            $table->foreign('user_id', 'user_follow_fk')->on('users')
                ->references('id')->onDelete('cascade');

            $table->unsignedBigInteger('follower_id')->nullable();
            $table->index('follower_id', 'follower_user_idx');
            $table->foreign('follower_id', 'follower_user_fk')->on('users')
                ->references('id')->onDelete('cascade');
            $table->boolean('followed')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('followers');
    }
};
