<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.store
     *
     * @return void
     */
    public function up()
    {
        Schema::create('twits', function (Blueprint $table) {
            $table->id();
            $table->text('text',255);
            $table->boolean('is_published')->default(1);
            $table->boolean('only_for_followers')->default(0);
            $table->unsignedBigInteger('user_id')->nullable();
            $table->index('user_id', 'twit_user_idx');
            $table->foreign('user_id', 'twit_user_fk')->on('users')
                ->references('id')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('twits');
    }
};
