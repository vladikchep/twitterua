<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('likes', function (Blueprint $table) {

            $table->id();

            $table->unsignedBigInteger('user_id')->nullable();
            $table->index('user_id', 'like_user_idx');
            $table->foreign('user_id', 'like_user_fk')->on('users')
                ->references('id')->onDelete('cascade');
            $table->unsignedBigInteger('twit_id')->nullable();
            $table->index('twit_id', 'like_twit_idx');
            $table->foreign('twit_id', 'like_twit_fk')->on('twits')
                ->references('id')->onDelete('cascade')->di;
//            $table->unique(array('twit_id','user_id'));

            $table->boolean('like')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('likes');
    }
};
