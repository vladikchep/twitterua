$(document).ready(function() {
    $(document).on('click', '.fButton', function (e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: '/follow',
            context : $(this),
            data: {
                user_id: parseInt($(this).attr("id")),
                followed: parseInt($(this).val()),
            },
        }).done(function (data) {
            if($(this).val()==parseInt(0)){
                $(this).val(parseInt(1));
                $(this).text('Followed');
            }
            else {
                $(this).val(parseInt(0));
                $(this).text('Follow');
            }

        })
    });
});
