<?php

use App\Http\Controllers\FollowerController;
use App\Http\Controllers\LikeController;
use App\Http\Controllers\TwitController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();
Route::middleware('auth')->group(function () {
    Route::controller(TwitController::class)->name('twit.')->group(function () {
        Route::get('/', 'index')->name('index');
        Route::get('/create', 'create')->name('create');
        Route::post('/', 'store')->name('store');
        Route::get('/{twit}/edit', 'edit')->name('edit');
        Route::patch('/{twit}', 'update')->name('update');
        Route::delete('/{twit}', 'destroy')->name('delete');
    });
    Route::prefix('user')->controller(UserController::class)->name('user.')->group(function () {
        Route::get('/{user}', 'show')->name('index');
        Route::get('/{user}/edit', 'edit')->name('edit');
        Route::patch('/{user}', 'update')->name('update');
    });
    Route::post('/like', [LikeController::class, 'like'])->name('like');
    Route::post('/follow', [FollowerController::class, 'follow'])->name('follow');

});






