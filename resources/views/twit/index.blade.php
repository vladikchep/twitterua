@extends('layouts.app')

@section('content')
    @foreach($twits as $twit)
        @can('view', $twit)
            <div class="card"
             style="display: block;  width: 30%;  margin: 1% auto;text-align: center;vertical-align: middle;">
            <div class="card-body">
                <x-delete-edit-buttons :twit=$twit />
                <div class="grid-containert" style="display: inline-flex;">
                    <div class="grid-itemt " style="vertical-align: middle;">
                        <a href="{{route('user.index',$twit->author->id)}}}" style="text-decoration: none;">
                            <h7 class="card-title"
                                style="text-align: left;">{{$twit->author->first_name}} {{$twit->author->last_name}}</h7>
                        </a>
                    </div>
                    <div class="grid-itemt m-xl-2" style="vertical-align: middle;">
                        <h6 class="card-subtitle mb-2 text-muted">    {{$twit->created_at}}</h6>
                    </div>
                </div>
            </div>
                <div><h6 class="card-subtitle mb-2 text-muted">{{$twit->text}}</h6></div>
            <x-like :twit=$twit />
        </div>
        @endcan
    @endforeach()
@endsection
@section('script')
    <script src="{{asset('js/like.js')}}"></script>
@endsection


