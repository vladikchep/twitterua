@extends('layouts.app')

@section('content')
    <div>
        <a href="{{route('twit.index')}}" style="text-decoration: none;">
            <button type="button" class="btn btn-outline-secondary"><- Back</button>
        </a>
    </div>

    <form action="{{route('twit.update',$twit->id)}}" method="post" style="width: 40%;
  margin: 0 auto;">
        @csrf
        @method('patch')

        <div class="mb-3">
            <label for="text" class="form-label">Text</label>
            <textarea class="form-control" name="text" id="text" rows="6" style="height:100%;width: 80%;"
                      placeholder="Content">{{old('text', isset($twit->text) ? $twit->text : '')}}</textarea>
            @error('text')
            <p class="text-danger">{{$message}}</p>
            @enderror
        </div>
        <div class="mb-3">
            <div class="form-check">
                <input class="form-check-input" type="checkbox"  name="only_for_followers" value="{{old('only_for_followers', isset($twit->only_for_followers) ? $twit->only_for_followers : 1) }}" id="flexCheckDefault">
                <label class="form-check-label" for="flexCheckDefault">
                    Only for followers
                </label>
            </div>
        </div>
        <div class="mb-3">
            <label for="party" class="mb-3">Schedule posting the twit:</label><br>
            <input id="party" type="datetime-local" name="partydate" value="">
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
@endsection
