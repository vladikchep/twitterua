@extends('layouts.app')

@section('content')
    <div class="m-4">
        <a href="{{route('twit.index')}}" style="text-decoration: none;">
            <button type="button" class="btn btn-outline-dark">Back to twits</button>
        </a>
    </div>
    @include('user.includes.info')
    <hr>
    <div class="grid-container">
        <div class="grid-item">
            @include('user.includes.followers')
        </div>
        <div class="grid-item">
            @include('user.includes.user_twits')
        </div>
        <div class="grid-item">
            @include('user.includes.followings')
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('js/follow.js')}}"></script>
@endsection
