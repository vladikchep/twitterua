<div class="card"
     style="display: block;  width: 40%;  height: 25%; margin: 1% auto;text-align: center;vertical-align: middle;">
    @can('update', $user)
        <div class="">
            <a href="{{route('user.edit',$user->id)}}" style="text-decoration: none;">
                <button type="button" class="btn btn-outline-dark">Edit profile</button>
            </a>
        </div>
    @endcan
    <a href="{{$user->avatar_url}}" style="text-decoration: none;">
        <img src="{{$user->avatar_url_thumb}}" class="m-4">
    </a>
    <h4>User profile:</h4><br>
    First name: {{$user->first_name}}<br>
    Last name: {{$user->last_name}}<br>
    Email: {{$user->email}}<br>
    On web site since: {{$user->created_at}}<br>
    @if($user->id!=Auth::id())
        @if($user->isFollowedBy(Auth::user())!=0)
            <button value="{{$user->isFollowedBy(Auth::user())}}" id="{{(int)$user->id}}" class="fButton m-4">
                Followed
            </button>
        @else
            <button
                value="{{(int)$user->isFollowedBy(Auth::user())}}" id="{{(int)$user->id}}" class="fButton m-4">
                Follow
            </button>
        @endif
    @endif
</div>
