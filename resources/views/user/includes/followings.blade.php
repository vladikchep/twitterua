Following:
@if(count($user->followingOn)>0)
    <div class="m-3"
         style="display: block;    height: 20%; margin: 1% auto;text-align: center;vertical-align: middle;">
        <ol class="c" style="list-style-type: decimal;">
        @foreach($user->followingOn as $follower)
                <li><a href="{{route('user.index',$follower->id)}}}" style="text-decoration: none;">
                        {{$follower->first_name}} {{$follower->last_name}}</a></li>
                {{$follower->email}}<br>
        @endforeach
        </ol>
    </div>
@else
    <h5>No yet.</h5>
@endif
