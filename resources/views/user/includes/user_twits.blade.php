@if(count($user->twits )>0)
    <h5 style="text-align: center;">Twits:</h5>
    @foreach($user->twits as $twit)

        @can('view', $twit)
            <div class="card"
                 style="  margin:  4%;text-align: center;vertical-align: middle;">

                <div class="card-body">
                    <x-delete-edit-buttons :twit=$twit />
                    <h7 class="card-title"
                        style="text-align: left;">{{$twit->author->first_name}} {{$twit->author->last_name}}</h7>
                    <h6 class="card-subtitle mb-2 text-muted">{{$twit->created_at}}</h6>
                    <p class="card-text">{{$twit->text}}</p>
                </div>
            </div>
        @endcan
    @endforeach
@else
    <h5 style="text-align: center;"> No twits.</h5>
@endif
