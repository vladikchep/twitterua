@extends('layouts.app')

@section('content')
    <div class="m-4">
        <a href="{{route('user.index',Auth::id())}}" style="text-decoration: none;">
            <button type="button" class="btn btn-outline-dark">Back to twits</button>
        </a>
    </div>
    <form action="{{route('user.update',$user->id)}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('patch')
        <div class="row mb-3">
            <label for="first_name" class="col-md-4 col-form-label text-md-end">{{ __('First name') }}</label>
            <div class="col-md-6">
                <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name', isset($user->first_name) ? $user->first_name : '') }}"  autocomplete="first_name" autofocus>
            </div>
        </div>
        <div class="row mb-3">
            <label for="last_name" class="col-md-4 col-form-label text-md-end">{{ __('Last name') }}</label>
            <div class="col-md-6">
                <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name', isset($user->last_name) ? $user->last_name : '') }}"  autocomplete="last_name" autofocus>
            </div>
        </div>
        <div class="row mb-3">
            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>
            <div class="col-md-6">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email', isset($user->email) ? $user->email : '') }}"  autocomplete="email">
            </div>
        </div>
        <div class="card" style="display: block;  width: 30%;  margin: 1% auto;text-align: center;vertical-align: middle;">
            <label class="form-label" for="customFile">Avatar image</label>
            <input type="file" class="form-control" id="customFile" name="avatar"/>
            <button type="submit" class="btn btn-primary">confirm</button>
        </div>
    </form>
@endsection
