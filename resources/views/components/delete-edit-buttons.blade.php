<div>
    <div style="float:left;">
        @can('update', $twit)
        <div class="" style="float: left;">
            <a href="{{route('twit.edit',$twit->id)}}" style="text-decoration: none;">
                <button type="button" class="btn btn-outline-dark btn-sm">Edit</button>
            </a>
        </div>
        @endcan
        @can('delete', $twit)
        <div style="float: left;">
            <form action="{{route('twit.delete',$twit->id)}}" method="post">
                @csrf
                @method('delete')
                <input type="submit" class="btn btn-outline-danger btn-sm" value="Delete">
            </form>
        </div>
            @endcan
    </div>
</div>
