<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container" >
        <a href="{{route('twit.index')}}" style="text-decoration: none ;alignment: left;"><h3>TwitUA</h3></a>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ms-auto">
                @if(Auth::check())
                    <div class="">
                        <a href="{{route('twit.create')}}" style="text-decoration: none;">
                            <button type="button" class="btn btn-outline-dark">Create post</button>
                        </a>
                    </div>
                    <div class="m-xl-3">
                        <a href="{{route('user.index',Auth::user()->id)}}}" style="text-decoration: none;">
                            {{ Auth::user()->first_name}} {{ Auth::user()->last_name}}
                        </a>
                    </div>
                    @if(Auth::user()->avatar_url_thumb)
                        <div class="">
                            <a href="{{route('user.index',Auth::id())}}}" style="text-decoration: none;">
                                <img src="{{Auth::user()->avatar_url_thumb}}" class="rounded-circle"
                                     style="width: 50px;"
                                     alt="" >
                            </a>
                        </div>
                    @endif
                @endif
                <!-- Authentication Links -->
                @include('includes.authentication')
            </ul>
        </div>
    </div>
</nav>

